#Disable telemetry
ubuntu-report -f send no
sudo apt remove --purge popularity-contest -y
sudo systemctl disable apport.service

#Remove Amazon
sudo apt remove --purge ubuntu-web-launchers -y
#Remove Livepatch icon
sudo mv software-properties-livepatch.desktop /tmp/

#Remove games
sudo apt remove --purge aisleriot -y
sudo apt remove --purge gnome-mahjongg -y
sudo apt remove --purge gnome-mines -y
sudo apt remove --purge gnome-sudoku -y

#Remove Torrent stuff
sudo apt remove --purge transmission-common -y
# Remove Latex stuff
sudo apt remove --purge texlive-base -y
sudo apt remove --purge texlive-binaries -y

sudo apt update -y
sudo apt upgrade -y
sudo apt autoremove -y

#Install packages
sudo apt install git -y
sudo apt install vim -y
sudo apt install openjdk-11-jdk -y
sudo apt install tilix -y
sudo apt install emacs -y
sudo apt install binwalk -y
sudo apt install chromium-browser -y
sudo apt install hashcat -y
sudo apt install ruby -y
sudo apt install haskell-stack -y
sudo apt install haskell-debian-utils -y
sudo apt install gdb -y
sudo apt install gparted -y
sudo apt install curl -y
sudo apt install netcat -y
sudo apt libnetfilter-queue-dev -y
sudo apt install traceroute -y
sudo apt install htop -y
sudo apt install ffmpeg -y
sudo apt install mpg123 -y
sudo apt install net-tools -y
sudo apt install xclip -y
sudo apt install whois -y
sudo apt install firejail -y
sudo apt install squashfs-tools -y
sudo apt install dirb -y
sudo apt install gnome-tweaks -y
sudo apt install filemanager-actions -y
sudo apt install nautilus-actions -y

if [[ $(lsb_release -r | tr -d -c 0-9) -gt '1900' ]]
then
        sudo apt-get install gnome-shell-extension-system-monitoir -y
fi


#Installing Python pip
sudo apt install python3-pip python-pip -y
sudo apt install python3-venv -y
sudo -H pip3 install flask
sudo -H pip3 install pwn
sudo -H pip3 install selenium
sudo -H pip3 install urllib3
sudo -H pip3 install requests

sudo -H pip install pwn

# Snap
sudo snap install sublime-text --classic

# Update
sudo apt update -y
sudo apt upgrade -y
sudo apt autoremove -y

# Add default update script
echo -e '#!/bin/bash\nsudo apt update -y\nsudo apt upgrade -y\nsudo apt autoremove -y' | sudo tee /bin/update
sudo chown root /bin/update
sudo chmod u=rwx,g=rx,o=rx /bin/update

# Add my basic .bashrc aliases
echo "alias l='ls -lha'" >> ~/.bashrc
echo "alias rm='mv -t /tmp '" >> ~/.bashrc
echo "alias rrm='/bin/rm '" >> ~/.bashrc
echo "alias syslog='tail -f /var/log/syslog'" >> ~/.bashrc

printf "Finished.\n"
