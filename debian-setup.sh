if [ "$(whoami)" == "root" ];
then
    echo "Start ${0##*/}"
else
    echo "This script requires root permissions"
    exit 1
fi

apt install sudo -y

#Update
apt update -y
apt upgrade -y
apt autoremove -y

#Install packages
apt install man -y
apt install mysql-server mysql-workbench -y
apt install git -y
apt install keepass2 -y
apt install openjdk-11-jdk -y
apt install tweak -y
apt install handbrake -y
sudo apt install apache2 -y
sudo apt install php7.2 -y
apt install tasksel -y
apt install gparted -y
apt install gnome-panel -y
apt install netcat -y
#apt install sublime-text -y
apt install net-tools -y
apt install pwgen -y
apt install traceroute -y
apt install htop -y
apt install curl -y
apt install sqlitebrowser -y

apt install xdotool -y
apt show sshfs -y

apt install vim -y
#apt install eclipse -y
apt install tilix -y
apt install emacs -y

#Hacking/ScriptKiddiTools
apt install dirb -y
apt install nmap -y
apt install aircrack-ng -y
apt install netdiscover -y
apt install sqlmap -y
apt install binwalk -y
apt install hashcat -y
apt install gdb -y
apt install ipcalc -y

#Install media
apt install vlc -y
apt install calibre -y
apt install blender -y
apt install inkscape -y
apt install kazam -y
apt install freecad -y
apt install kdenlive -y
apt install ffmpeg -y
apt install mpg123 -y
apt install gimp


#Install browsers
apt install firefox -y
sudo apt install lynx -y
sudo apt install chromium-browser -y

#Install atom
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | apt-key add -
sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
apt update
apt install atom

#Install Python pip
sudo apt install python3-pip python-pip -y
sudo apt install python3-opencv -y
sudo apt install python-opencv -y
sudo -H pip3 install flask
sudo -H pip3 install pwn
sudo -H pip3 install selenium
sudo -H pip3 install urllib3
sudo -H pip3 install requests
sudo -H pip install pwn
pip3 install imutils --user

apt update -y
apt upgrade -y
apt autoremove -y

apt install wireshark -y

printf "Finished.\n"