if [ "$(whoami)" == "root" ];
then
    echo "START"
else
    echo "This script requires root permissions"
    exit 1
fi


apt install xorg -y
apt install i3 -y