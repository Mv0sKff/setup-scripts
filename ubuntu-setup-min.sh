#Disable telemetry
ubuntu-report -f send no
sudo apt remove --purge popularity-contest -y
sudo systemctl disable apport.service

#Update
sudo apt update -y
sudo apt upgrade -y

#Remove Amazon
sudo apt remove --purge ubuntu-web-launchers -y
#Remove Livepatch icon
sudo mv software-properties-livepatch.desktop /tmp/

#Remove games
sudo apt remove --purge aisleriot -y
sudo apt remove --purge gnome-mahjongg -y
sudo apt remove --purge gnome-mines -y
sudo apt remove --purge gnome-sudoku -y

#Remove Torrent stuff
sudo apt remove --purge transmission-common -y
# Remove Latex stuff
sudo apt remove --purge texlive-base -y
sudo apt remove --purge texlive-binaries -y

#Removing dependencies from packages above
sudo apt autoremove -y

#Install packages
sudo apt install git -y
sudo apt install vim -y
sudo apt install curl -y
sudo apt install xclip -y
sudo apt install net-tools -y
sudo apt install netcat -y
sudo apt install whois -y
sudo apt install traceroute -y
sudo apt install lynx -y
sudo apt install python3-pip python-pip -y
sudo apt install mpg123 -y
sudo apt install pwgen -y
sudo apt install emacs -y

printf "Finished.\n"