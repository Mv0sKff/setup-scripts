# Check for root privileges
if [ "$(whoami)" != "root" ];
then
    echo "This script requires root privileges!"
    exit 1
fi

# Editing config files and disable services
systemctl disable cups-browsed # Disable printer stuff

cp /etc/avahi/avahi-daemon.conf /etc/avahi/avahi-daemon.conf_original
sed -i '/\#disable-publishing=no/c\disable-publishing=yes' /etc/avahi/avahi-daemon.conf

# Configure firewall
ufw default deny incoming

# Disable telemetry on Ubuntu
ubuntu-report -f send no
apt remove --purge popularity-contest -y
systemctl disable apport.service

# Update
apt update -y
apt upgrade -y

# Remove Amazon
apt remove --purge ubuntu-web-launchers -y
# Remove Livepatch icon
mv software-properties-livepatch.desktop /tmp/
# Remove games
apt remove --purge aisleriot -y
apt remove --purge gnome-mahjongg -y
apt remove --purge gnome-mines -y
apt remove --purge gnome-sudoku -y

# Remove Torrent stuff
apt remove --purge transmission-common -y
# Remove Latex stuff
apt remove --purge texlive-base -y
apt remove --purge texlive-binaries -y

# Removing dependencies from packages above
apt autoremove -y

# Install packages
#apt install mysql-server mysql-workbench -y
#apt install php7.2 -y
apt install git -y
apt install keepassxc -y
apt install openjdk-11-jdk -y
#apt install tweak -y
apt install handbrake -y
apt install tasksel -y
apt install gparted -y
#apt install gnome-panel -y
apt install netcat -y
apt install tree -y
apt install xclip -y
apt install tree -y
#apt libnetfilter-queue-dev -y
#apt install apache2 -y
apt install traceroute -y
apt install ruby -y
#apt install sublime-text -y
apt install net-tools -y
apt install whois -y
apt install pwgen -y
apt install wget2 -y
apt install htop -y
apt install curl -y
apt install sqlitebrowser -y
apt install xdotool -y
apt install adb -y
apt install squashfs-tools -y
apt install sshfs -y
apt install gnome-tweaks -y
#apt install filemanager-actions -y
#apt install nautilus-actions -y
apt install okular -y
apt install imwheel -y
apt install adwaita-qt -y

#if [[ $(lsb_release -r | tr -d -c 0-9) -gt '1900' ]]
#then
#        apt install gnome-shell-extension-system-monitoir -y
#fi

apt install vim -y
apt install eclipse -y
apt install tilix -y
#apt install emacs -y

# Hacking/ScriptKiddiTools
apt install dirb -y
apt install nmap -y
apt install aircrack-ng -y
apt install netdiscover -y
apt install sqlmap -y
apt install firejail -y
apt install binwalk -y
apt install hashcat -y
apt install gdb -y
apt install ipcalc -y
apt install radare2 -y

# Install media
apt install vlc -y
apt install calibre -y
apt install blender -y
apt install inkscape -y
#apt install kazam -y
apt install freecad -y
apt install ffmpeg -y
apt install mpg123 -y

# Install python stuff
apt install python3-pip python-pip -y
apt install python3-venv -y
apt install python3-opencv -y
apt install python-opencv -y
pip3 install flask
pip3 install selenium
pip3 install urllib3
pip3 install requests
pip3 install numpy
pip3 install imutils --user
pip3 install pwn
pip install pwn
mv /usr/local/bin/update /usr/local/bin/pwntools_update

ln /usr/bin/python3 /usr/bin/python

# Install browsers
apt install firefox -y
apt install lynx -y
#apt install chromium-browser -y

# Install snaps
#snap install gimp
#snap install slack --classic
#snap install android-studio --classic
#snap install pycharm-community --classic 
#snap install zaproxy

# Install flatpaks
apt install flatpak -y
flatpak install chromium -y
flatpak install discord -y
flatpak install glimpse -y
flatpak install flathub io.atom.Atom -y
flatpak install flathub org.zaproxy.ZAP -y
flatpak install flathub com.jetbrains.PyCharm-Community -y
flatpak install flathub com.jetbrains.IntelliJ-IDEA-Community -y
flatpak install flathub org.godotengine.Godot -y
flatpak install flathub org.kde.kdenlive -y
flatpak install flathub io.github.seadve.Kooha -y
flatpak install flathub org.gnome.SoundRecorder -y
#flatpak install flathub com.github.xournalpp.xournalpp -y
#flatpak install flathub com.notepadqq.Notepadqq
#flatpak install flathub com.google.AndroidStudio
#flatpak install flathub com.slack.Slack
#flatpak install flathub org.processing.processingide
#flatpak install flathub com.valvesoftware.SteamLink
#flatpak install flathub io.mrarm.mcpelauncher

# Finishing up
apt update -y
apt upgrade -y
apt autoremove -y

# Add handy update script
printf '#!/bin/bash\napt update -y\napt upgrade -y\napt autoremove -y' > /bin/update
chown root /bin/update
chmod u=rwx,g=rx,o=rx /bin/update

# Add my basic .bashrc aliases
echo "alias l='ls -lha'" >> /root/.bashrc
echo "alias rm='mkdir -p /tmp/trash; mv -t /tmp/trash '" >> /root/.bashrc
echo "alias rrm='/bin/rm '" >> /root/.bashrc
echo "alias syslog='tail -f /var/log/syslog'" >> /root/.bashrc

defaultuser=$(who am i | awk '{print $1}')

echo "Adding .bashrc aliases also to $defaultuser"

echo "alias l='ls -lha'" >> /home/$defaultuser/.bashrc
echo "alias rm='mkdir -p /tmp/trash; mv -t /tmp/trash '" >> /home/$defaultuser/.bashrc
echo "alias rrm='/bin/rm '" >> /home/$defaultuser/.bashrc
echo "alias syslog='tail -f /var/log/syslog'" >> /home/$defaultuser/.bashrc

apt install wireshark -y

printf "Finished.\n"
