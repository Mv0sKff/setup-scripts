if [ "$(whoami)" == "root" ];
then
    echo "Start ${0##*/}"
else
    echo "This script requires root permissions"
    exit 1
fi

sudo apt update -y
sudo apt upgrade -y
sudo apt autoremove -y

sudo apt install git -y
sudo apt install vim -y
sudo apt install curl -y
sudo apt install xclip -y
sudo apt install net-tools -y
sudo apt install netcat -y
sudo apt install whois -y
sudo apt install traceroute -y
sudo apt install lynx -y
sudo apt install python3-pip python-pip -y
sudo apt install mpg123 -y
sudo apt install pwgen -y
sudo apt install emacs -y
sudo apt install tasksel -y
sudo apt install php7.2 -y
sudo apt install ruby -y
sudo apt install tshark -y
sudo apt install sshfs -y
sudo apt install emacs -y
sudo apt install dirb -y
sudo apt install nmap -y
sudo apt install aircrack-ng -y
sudo apt install netdiscover -y
sudo apt install sqlmap -y
sudo apt install firejail -y
sudo apt install binwalk -y
sudo apt install hashcat -y
sudo apt install gdb -y
sudo apt install ipcalc -y
sudo apt install radare2 -y
sudo apt install ffmpeg -y
sudo apt install python3-venv -y
sudo apt install lynx -y

printf "Finished.\n"