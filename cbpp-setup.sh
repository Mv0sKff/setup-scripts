#Update
sudo apt update -y
sudo apt upgrade -y
sudo apt autoremove -y

#Install packages
sudo apt install git -y
sudo apt install vim -y
sudo apt install openjdk-11-jdk -y
sudo apt install tilix -y
sudo apt install freecad -y
sudo apt install kdenlive -y
sudo apt install tweak -y
sudo apt install wireshark -y
sudo apt install aircrack-ng -y
sudo apt install netdiscover -y
sudo apt install gparted -y
sudo apt install xdotool -y
sudo apt install curl -y
sudo apt install netcat -y
sudo apt install nmap -y
sudo apt install ffmpeg -y
sudo apt install mpg123 -y
sudo apt install net-tools -y
sudo apt install pwgen -y
#sudo apt install sublime-text -y
sudo apt install dirb -y

#Install media packages
sudo apt install vlc -y
sudo apt install calibre -y

#Install atom
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | apt-key add -
sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
sudo apt update
sudo apt install atom

sudo apt update -y
sudo apt upgrade -y
sudo apt autoremove -y

echo "Finished."
echo 