#!/bin/bash

# This script is part of the Mv0sKff setup-scripts repository.
# Licence: https://gitlab.com/Mv0sKff/setup-scripts/raw/master/LICENSE

#Array for all packages!
declare -a packageList
packageList=(vim git net-tools pwgen)


welcomePage() {
    #clear
    echo "
          _                      _ _      _    
  ___ ___| |_ _  _ _ __  ___  __| (_)  __| |_  
 (_-</ -_)  _| || | '_ \|___|/ _| | |_(_-< ' \ 
 /__/\___|\__|\_,_| .__/     \__|_|_(_)__/_||_|
                  |_|                           
	" 
}

setup() {
	 mkdir -p /tmp/setup-cli-logs
}

installDialog() {
	if [[ $2 != n ]]
	then
		printf "Would you like to install $1? [Y/n] "
#	elif [[ $3 == m ]]
#	then
#		echo "If you choose \'Yes\' those packages will be installed [Y/n] "
	else
		printf "Would you like to install $1? [N/y] "
	fi
	read q

	if [[ $q == y ]] || [[ $q == Y ]] || ([[  $q == "" ]] && [[  $2 != n ]])
	then
		printf '\n'
		echo "Installing $1!" 
		printf '\n'	
		sudo apt install $1 -y
	elif [[ $q == n ]] || [[ $q == N ]]
	then
		echo "NOT installing $1"
	else
		echo "Invalid input, try again"
		printf '\n'
		installDialog $1 $2
	fi
	
}

welcomePage

#echo ${packageList[@]}

for i in "${packageList[@]}"
do
 	echo #$i
	installDialog $i
done

#welcomePage
#installDialog "vim" ""
#setup