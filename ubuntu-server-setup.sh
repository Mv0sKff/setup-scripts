# Check for root privileges
if [ "$(whoami)" != "root" ];
then
    echo "This script requires root privileges"
    exit 1
fi

# Update
apt update -y
apt upgrade -y
apt autoremove -y

# Install packages
apt install mysql-server mysql-workbench -y
apt install git -y
apt install tasksel -y
apt install php7.2 -y
apt install apache2 -y
apt install traceroute -y
apt install net-tools -y
apt install pwgen -y
apt install htop -y
apt install curl -y
apt install squashfs-tools
apt install sshfs -y
apt install vim -y
apt install dirb -y
apt install nmap -y
apt install netdiscover -y
apt install lynx -y

apt install python3-pip -y
apt install python-pip -y
apt install python3-venv -y

pip3 install flask

ln /usr/bin/python3 /usr/bin/python

# Add useful update script
printf '#!/bin/bash\napt update -y\napt upgrade -y\napt autoremove -y' > /bin/update
chown root /bin/update
chmod u=rwx,g=rx,o=rx /bin/update

# Add my basic .bashrc aliases
echo "alias l='ls -lha'" >> /root/.bashrc
echo "alias rm='mkdir -p /tmp/trash; mv -t /tmp/trash '" >> /root/.bashrc
echo "alias rrm='/bin/rm '" >> /root/.bashrc
echo "alias syslog='tail -f /var/log/syslog'" >> /root/.bashrc

defaultuser=$(who am i | awk '{print $1}')

echo "Adding .bashrc aliases also to $defaultuser"

echo "alias l='ls -lha'" >> /home/$defaultuser/.bashrc
echo "alias rm='mkdir -p /tmp/trash; mv -t /tmp/trash '" >> /home/$defaultuser/.bashrc
echo "alias rrm='/bin/rm '" >> /home/$defaultuser/.bashrc
echo "alias syslog='tail -f /var/log/syslog'" >> /home/$defaultuser/.bashrc

#su $defaultuser -c "source /home/$defaultuser/.bashrc"

printf "Finished.\n"